import React from 'react';
import { NavLink } from 'react-router-dom';
import './Person.css';

let Person = (props) => {
    return (
        <div className="person rounded columns">
            <div className="column col-3">
                <img src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" alt="logo" className="modelAvatar" />
            </div>
            <div className="column col-9 tile">
                <div className="tile-content">
                    <p className="tile-title"><b>{props.data.name}</b></p>
                    <p className="tile-subtitle text-gray">Earth's Mightiest Heroes joined forces to take on threats that were too big for any one hero to tackle...</p>
                </div>
            </div>
            <div className="column col-3">
                <NavLink to={`/models/${props.data.id}`} data={props.data}><button className="btn btn-primary btn-full">View</button></NavLink>
            </div>
        </div>
    )
}

export default Person;