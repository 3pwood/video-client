import React from 'react';
import Person from './Person';

let PeopleList = (props) => {
    return (
        <div>
            {props.people.map(p =>
                <Person key={p.id} data={p} />
            )}
        </div>
    )
};

export default PeopleList;