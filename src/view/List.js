import React from 'react';
import axios from 'axios';

import Person from '../components/personList/Person';
import PeopleList from '../components/personList/PeopleList';

class List extends React.Component {

    state = {
        people: []
    };

    componentWillMount() {
        axios
        .get('http://192.168.1.21:8000/people')
        .then(res => {
            // console.log(res.data);
            const newPeople = res.data.map(p => {
                return { id: p.id, name: p.firstname + ' ' + p.lastname };
            });
            this.setState({ people: newPeople })
        })
        .catch(err => console.log(err));
    }
    
    render() {
        return (
            <div>
                <Person data={{id: 1, name: 'Model #1'}} />
                <Person data={{id: 2, name: 'Model #2'}} />
                <Person data={{id: 3, name: 'Model #3'}} />
                <Person data={{id: 4, name: 'Model #4'}} />
                <Person data={{id: 5, name: 'Model #5'}} />
                <PeopleList people={this.state.people} />
            </div>
        )
    }   
}

export default List;