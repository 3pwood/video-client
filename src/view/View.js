import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from '../view/Home';
import List from './List';
import Contact from './Contact';

let View = () => (
    <main className='container'>
        <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/models' exact component={List} />
            <Route path="/models/:modelsId" component={Contact} data/>
        </Switch>
    </main>
)

export default View