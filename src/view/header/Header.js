import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import logo from './logo.svg';
import './Header.css';

class Header extends React.Component {
    constructor() {
        super();
        this.state = { hidden: true }

        this.toggle = this.toggle.bind(this)
        this.close = this.close.bind(this)
    }

    toggle() {
        this.setState({ hidden: !this.state.hidden });
    }

    close() {
        this.setState({hidden: true});
    }

    render() {
        return (
            <header className="navbar bg-secondary">
                <section className="navbar-section">
                    <Link to="/" className="navbar-brand mr-2">
                        <img src={logo} className="r-logo" alt="logo" />
                    </Link>
                    <NavLink exact to="/" className="btn btn-link ij-effect-13" activeClassName='active'>Home</NavLink>
                    <NavLink to="/models" className="btn btn-link ij-effect-13" activeClassName='active'>Models</NavLink>
                </section>
                <section className="navbar-section">
                    <div className="has-icon-right">
                        <input type="text" className="form-input-sm" placeholder="Search" />
                        <i className="form-icon icon icon-search"></i>
                    </div>
                    <figure className="avatar" onClick={this.toggle}>
                        <img src="https://cdn0.iconfinder.com/data/icons/20-flat-icons/128/user.png" alt='' />
                    </figure>
                    {!this.state.hidden && <Profile user={this.props.user} toggle={this.toggle} close={this.close}/>}
                </section>
            </header>
        )
    }
}

let Profile = (props) => (
    <div>
        <span className="off-canvas" onClick={props.close}></span>
        <ul className="menu bg-secondary">
            <li className="menu-item text-center text-bold">{props.user}</li>
            <li className="divider"></li>
            <li className="menu-item">
                <div className="menu-badge">
                    <i className="icon icon-link"></i>
                </div>
                <Link to="/" onClick={props.toggle}>Settings</Link>
            </li>
        </ul>
    </div>
)

export default Header;