import React from 'react';
import './App.css';
import '../node_modules/spectre.css/dist/spectre.min.css';
import '../node_modules/spectre.css/dist/spectre-exp.min.css';
import '../node_modules/spectre.css/dist/spectre-icons.min.css';

import Header from './view/header/Header';
import View from './view/View';

let App = () => (
    <div>
        <Header user="Profile" />
        <View />
    </div>
);

export default App;
